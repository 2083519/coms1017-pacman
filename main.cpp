#include <iostream>
#include <thread>
#include <chrono>

#include "window.h"
#include "texture.h"
#include "tile.h"
#include "helpers.h"

using namespace std;

int main()
{

    // SpriteSheet Filename
    string spriteFilename = SPRITEFILENAME; // Leave this line

    // Setup and Load Texture object here
    bool quit = false;

    //Declaring texture myTextures
    Texture myTexture;

    //frame var
    int frame = 0;

    //getTestTiles
    vector<Tile> testTiles = getTestTiles();

    myTexture.loadFile(spriteFilename, 20, 20);
    Tile pm(0,0,{ {1,1}, {1,2}, {1,1}, {1,3}},Pacman,1,1);
    while(!quit){
        // Handle any SDL Events
        // Such as resize, clicking the close button,
        //  and process and key press events.
        SDL_Event e;
        while(SDL_PollEvent(&e)){
            if(e.type == SDL_QUIT){
                quit = true;
            }
        }

        // Update the Game State Information

        // Draw the current state to the screen.

            //Clear Everything on the screen
        SDL_RenderClear(myTexture.myWin.sdlRenderer);
        SDL_RenderPresent(myTexture.myWin.sdlRenderer);
        SDL_SetRenderDrawColor(myTexture.myWin.sdlRenderer, 0, 0, 0, 255);

            //Drawing sprites
        // Set background to black
        SDL_SetRenderDrawColor(myTexture.myWin.sdlRenderer, 0, 0, 0, 0xFF);
        // Clear the renderer
        SDL_RenderClear(myTexture.myWin.sdlRenderer);
        // Render the tile
        //pm.render(&myTexture, frame);

        int a = testTiles.size();
        for(auto i = 0; i < a;i++){
                Tile temp = testTiles[i];
                temp.render(&myTexture, frame);
        }

        // Copy the memory of the renderer to the window
        SDL_RenderPresent(myTexture.myWin.sdlRenderer);

        //Incremet frame by 1
        frame++;
        this_thread::sleep_for(chrono::milliseconds(75));
    }

    return 0;
}
